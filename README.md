# **HomeRun** #

## Um das Spiel zu starten befolgen Sie bitte folgende Schritte: ##



1. Laden Sie sich den Sourcecode von BitBucket herunter und installieren Sie **node.js**.

1. In Ihrer Node.js command prompt geben Sie dann "**npm install http-server -g**" ein. 

1. Im Folgenden Schritt navigieren Sie zu Ihrem HomeRun Verzeichnis, Sie geben beispielsweise "**cd C:\Users\Lukas Wicher\Documents\homerun**" ein.

1. Jetzt nur noch "**http-server**" in die Node.js command prompt eingeben und bestätigen und das Spiel sollte unter **localhost:8080/index.html** spielbar sein.

1. Alternativ können Sie auch einen Editor wie z.B. **Brackets** installieren, welcher einen lokalen Live Server für Sie zur Verfügung stellt.