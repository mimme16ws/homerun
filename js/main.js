var HomeRun = HomeRun || {};
var Phaser;

/* eslint-env browser */

/* Diese Methode initialisiert das Spiel und die Game States, skaliert die Spielwelt auf 800x600 Pixel und startet den Bootvorgang */

HomeRun.game = new Phaser.Game(800, 600, Phaser.AUTO, "");

HomeRun.game.state.add("Boot", HomeRun.Boot);
HomeRun.game.state.add("Preload", HomeRun.Preload);
HomeRun.game.state.add("Game", HomeRun.Game);
HomeRun.game.state.add("StartScreen", HomeRun.StartScreen);
HomeRun.game.state.add("LevelScreen", HomeRun.LevelScreen);
HomeRun.game.state.add("SettingsScreen", HomeRun.SettingsScreen);
HomeRun.game.state.add("HighscoreScreen", HomeRun.HighscoreScreen);
HomeRun.game.state.add("CreditsScreen", HomeRun.CreditsScreen);
HomeRun.game.state.add("GameOver", HomeRun.GameOver);
HomeRun.game.state.add("LevelCompleted", HomeRun.LevelCompleted);

HomeRun.game.state.start("Boot");