var HomeRun = HomeRun || {};

/* eslint-env browser */

/* In dieser Methode werden Informationen über das Spiel "HomeRun" angezeigt */

(function () {
    "use strict";

    HomeRun.CreditsScreen = function () {};

    HomeRun.CreditsScreen.prototype = {
        init: function (chosenCharacter, musikActivated, soundActivated) {
            this.chosenCharacter = chosenCharacter;
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
        },
        
        preload: function () {},

        create: function () {
            this.startScreenBackground = this.add.sprite(400, 300, "startScreen");
            this.startScreenBackground.anchor.setTo(0.5, 0.5);
            
            this.zurueckButton = this.game.add.button(220, 100, "zurueck", this.zurueckButtonOnClick, this);
            this.zurueckButton.anchor.setTo(1, 1);

            this.mmeProjekt = this.add.sprite(220, 270, "mmeProjekt");
            this.mmeProjekt.scale.setTo(0.5, 0.5);
        },

        zurueckButtonOnClick: function () {
            this.state.start("StartScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        }
    };
    
}());