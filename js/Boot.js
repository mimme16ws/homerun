var HomeRun = HomeRun || {};
var Phaser;

/* eslint-env browser */

/* In dieser Methode wird der Browserhintergrund sowie der Hauptbildschirm ins Spiel geladen, die Spielwelt in die Mitte gerendert und die Phaser Game Engine gestartet. Desweiteren wird der Preload-State aufgerufen, in welchem alle Assets geladen werden. */

(function () {
    "use strict";

    HomeRun.Boot = function () {};

    HomeRun.Boot.prototype = {
        preload: function () {
            this.game.stage.backgroundColor = "#FFFFFF";
            this.load.image("loadingScreen", "assets/StartScreen/TVStartScreen.png");
        },
        
        create: function () {
            this.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            this.scale.pageAlignHorizontally = true;
            this.scale.pageAlignVertically = true;
            
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            
            this.state.start("Preload");
        }
    };

}());