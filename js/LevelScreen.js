var HomeRun = HomeRun || {};

/* eslint-env browser */

/* In dieser Methode hat man die Möglichkeit zwischen drei verschiedenen Leveln sowie einem Tutorial auszuwählen, vorab ist es empfehlenswert das Tutorial mit den Spielanweisungen zu spielen */

(function () {
    "use strict";

    HomeRun.LevelScreen = function () {};

    HomeRun.LevelScreen.prototype = {
        init: function (chosenCharacter, musikActivated, soundActivated) {
            this.chosenCharacter = chosenCharacter;
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
        },
        
        preload: function () {},
        
        create: function () {
            this.startScreenBackground = this.add.sprite(400, 300, "startScreen");
            this.startScreenBackground.anchor.setTo(0.5, 0.5);

            this.zurueckButton = this.game.add.button(220, 100, "zurueck", this.zurueckButtonOnClick, this);
            this.zurueckButton.anchor.setTo(1, 1);

            this.levelOneButton = this.game.add.button(400 - 10, 300 - 20, "levelOne", this.levelOneButtonOnClick, this);
            this.levelOneButton.scale.setTo(0.2, 0.2);
            this.levelOneButton.anchor.setTo(0.5, 0.5);

            this.levelTwoButton = this.game.add.button(400 - 10, 300 + 40, "levelTwo", this.levelTwoButtonOnClick, this);
            this.levelTwoButton.scale.setTo(0.2, 0.2);
            this.levelTwoButton.anchor.setTo(0.5, 0.5);

            this.levelThreeButton = this.game.add.button(400 - 10, 300 + 100, "levelThree", this.levelThreeButtonOnClick, this);
            this.levelThreeButton.scale.setTo(0.2, 0.2);
            this.levelThreeButton.anchor.setTo(0.5, 0.5);
            
            this.tutorialButton = this.game.add.button(400 - 10, 300 + 160, "tutorial", this.tutorialButtonOnClick, this);
            this.tutorialButton.scale.setTo(0.2, 0.2);
            this.tutorialButton.anchor.setTo(0.5, 0.5);
        },

        levelOneButtonOnClick: function () {
            this.state.start("Game", true, false, 1, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        levelTwoButtonOnClick: function () {
            this.state.start("Game", true, false, 2, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        levelThreeButtonOnClick: function () {
            this.state.start("Game", true, false, 3, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        tutorialButtonOnClick: function () {
            this.state.start("Game", true, false, 4, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        zurueckButtonOnClick: function () {
            this.state.start("StartScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        }
    };

}());