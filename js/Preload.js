var HomeRun = HomeRun || {};
var Phaser;

/* eslint-env browser */

/* In dieser Methode werden alle zum Spiel gehörenden Bilder, Schriftzüge, Tilemaps, Objekte, Charaktere, Buttons und Audio-Dateien aus dem Assets Ordner geladen und für die Spiellogik bereitgestellt. */

(function () {
    "use strict";

    HomeRun.Preload = function () {};

    HomeRun.Preload.prototype = {
        preload: function () {
            //Ladescreen lädt das Spiel während alle Bilder im Hintergrund verarbeitet werden
            this.preStartScreen = this.add.sprite(this.game.world.centerX, this.game.world.centerY, "loadingScreen");
            this.preStartScreen.anchor.setTo(0.5, 0.5);
            this.load.setPreloadSprite(this.preStartScreen);

            //Lädt alle mit Tiled erstellten Tilemaps bzw. Spielwelten
            this.load.tilemap("firstLevel", "assets/Tilemaps/mapLevel1.json", null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap("secondLevel", "assets/Tilemaps/mapLevel2.json", null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap("thirdLevel", "assets/Tilemaps/mapLevel3.json", null, Phaser.Tilemap.TILED_JSON);
            this.load.tilemap("tutorialLevel", "assets/Tilemaps/tutorialTilemap.json", null, Phaser.Tilemap.TILED_JSON);

            //Lädt alle zu den Tilemaps gehörigen Referenzbilder
            this.load.image("TutorialTilemap", "assets/Tilemaps/TutorialTilemap.png");
            this.load.image("gameTiles", "assets/Tilemaps/gameTiles.png");
            this.load.image("gameTiles2", "assets/Tilemaps/gameTiles2.png");
            this.load.image("gameTiles3", "assets/Tilemaps/gameTiles3.png");

            //Lädt alle im Spiel vorkommenden Objekte
            this.load.image("donut", "assets/Objekte/donut.png");
            this.load.image("falle", "assets/Objekte/falle.png");
            this.load.image("bigDonut", "assets/Objekte/bigDonut.png");
            this.load.image("atomstab", "assets/Objekte/atomstab.png");

            /* Lädt alle im Spiel vorkommenden Buttons */
            this.load.image("spielen", "assets/Buttons/spielenButton.png");
            this.load.image("highscore", "assets/Buttons/highscoreButton.png");
            this.load.image("settings", "assets/Buttons/einstellungenButton.png");
            this.load.image("credits", "assets/Buttons/creditsButton.png");
            this.load.image("zurueck", "assets/Buttons/zurueckButton.png");
            this.load.image("neustart", "assets/Buttons/neustartButton.png");
            this.load.image("hauptmenue", "assets/Buttons/hauptmenueButton.png");
            
            this.load.image("levelOne", "assets/Buttons/Level1.png");
            this.load.image("levelTwo", "assets/Buttons/Level2.png");
            this.load.image("levelThree", "assets/Buttons/Level3.png");
            this.load.image("tutorial", "assets/Buttons/tutorialButton.png");
            
            /* Lädt den Startbildschirm ins Spiel */
            this.load.image("startScreen", "/assets/StartScreen/TVStartScreenClouds.png");
            
            /* Settingsscreen Buttons */
            this.load.image("musikButton", "/assets/Buttons/musikButton.png");
            this.load.image("soundButton", "/assets/Buttons/soundButton.png");
            this.load.image("musikButtonDeaktiviert", "/assets/Buttons/musikButtonDeaktiviert.png");
            this.load.image("soundButtonDeaktiviert", "/assets/Buttons/soundButtonDeaktiviert.png");

            /* Lädt den auf dem Creditsscreen angezeigten Schriftzug */
            this.load.image("mmeProjekt", "/assets/Schriftzüge/mmeProjekt.png");
        
            /* Lädt die Ansicht der Charaktere für den Menüpunkt Einstellungen */
            this.load.spritesheet("chooseHomerHover", "/assets/Charaktere/homerCharacterHover.png", 103, 204);
            this.load.spritesheet("chooseMargeHover", "/assets/Charaktere/margeCharacterHover.png", 103, 204);
            this.load.spritesheet("chooseBartHover", "/assets/Charaktere/bartCharacterHover.png", 103, 204);
            
            /* Lädt alle im Spiel vorkommenden Schriftzüge */
            this.load.image("charakterauswahl", "/assets/Schriftzüge/charakterauswahl.png");
            this.load.image("clickHinweise", "/assets/Schriftzüge/clickHinweise.png");
            this.load.image("mmeProjekt", "/assets/Schriftzüge/mmeProjekt.png");
            this.load.image("spielAnweisungen", "/assets/Schriftzüge/spielAnweisungen.png");
            this.load.image("gameOver", "/assets/Schriftzüge/gameOver.png");
            this.load.image("levelCompleted", "/assets/Schriftzüge/levelCompleted.png");

            /* Lädt die Hintergrundmusik des Spiels sowie den Sound wenn der Charakter stirbt */
            this.game.load.audio("simpsonsTheme", ["assets/Audio/simpsonsSoundtrack.mp3",  "assets/audio/simpsonsSoundtrack.ogg"]);
            this.game.load.audio("homerDeathSound", ["assets/Audio/homerDeathSound.mp3", "assets/audio/homerDeathSound.ogg"]);
            
            /* Lädt die Bilder, die angezeigt werden, wenn der Charakter stirbt */
            this.load.image("homerDeath", "/assets/Charaktere/homerDeath.png");
            this.load.image("margeDeath", "/assets/Charaktere/margeDeath.png");
            this.load.image("bartDeath", "/assets/Charaktere/bartDeath.png");

            /* Lädt die Spritesheets aller im Spiel vorkommenden Charaktere und Gegner */
            this.load.spritesheet("enemy", "assets/Charaktere/tingeltangelbobSpritesheet.png", 41, 79);
            this.load.spritesheet("homer", "assets/Charaktere/homer_spritesheet.png", 76, 150);
            this.load.spritesheet("bart", "assets/Charaktere/bart_spritesheet.png", 36, 93);
            this.load.spritesheet("marge", "assets/Charaktere/marge_spritesheet.png", 63, 180);
            this.load.spritesheet("homerPowerup", "assets/Charaktere/homerpowerup_spritesheet.png", 76, 150);
            this.load.spritesheet("bartPowerup", "assets/Charaktere/bartpowerup_spritesheet.png", 36, 93);
            this.load.spritesheet("margePowerup", "assets/Charaktere/margepowerup_spritesheet.png", 63, 180);
        },

        /* Ruft das Hauptmenü nach Laden aller Assets auf */
        
        create: function () {
            this.state.start("StartScreen");
        }
    };
    
}());