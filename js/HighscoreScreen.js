var HomeRun = HomeRun || {};

/* eslint-env browser */

/* Zu diesem Screen gelangt man, indem man im Startmenü auf den Button Highscore klickt, hier sieht man die im Local Storage gespeicherten Scores der einzelnen Level. Diese werden über die Getter-Methode des Local Storage geholt. Nach Browserneustart werden alle Highscores wieder auf 0 gesetzt. */

(function () {
    "use strict";

    HomeRun.HighscoreScreen = function () {};

    HomeRun.HighscoreScreen.prototype = {
        init: function (chosenCharacter, musikActivated, soundActivated) {
            this.chosenCharacter = chosenCharacter;
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
        },
        
        preload: function () {
            this.map1highscore = 0;
            this.map2highscore = 0;
            this.map3highscore = 0;
        },

        /* Holen der gespeicherten Daten aus dem Local Storage */
        
        create: function () {
            if (localStorage.getItem("highScoreLevel1")) {
                this.map1highscore = localStorage.getItem("highScoreLevel1");
            }
            if (localStorage.getItem("highScoreLevel2")) {
                this.map2highscore = localStorage.getItem("highScoreLevel2");
            }
            if (localStorage.getItem("highScoreLevel3")) {
                this.map3highscore = localStorage.getItem("highScoreLevel3");
            }
            this.startScreenBackground = this.add.sprite(400, 300, "startScreen");
            this.startScreenBackground.anchor.setTo(0.5, 0.5);
            
            this.levelOneScoreText = this.add.text(300, 300, "Level 1: " + this.map1highscore, { fontSize: "32px", fill: "#000000" });
            this.levelTwoScoreText = this.add.text(300, 350, "Level 2: " + this.map2highscore, { fontSize: "32px", fill: "#000000" });
            this.levelThreeScoreText = this.add.text(300, 400, "Level 3: " + this.map3highscore, { fontSize: "32px", fill: "#000000" });

            this.zurueckButton = this.game.add.button(220, 100, "zurueck", this.zurueckButtonOnClick, this);
            this.zurueckButton.anchor.setTo(1, 1);
        },

        zurueckButtonOnClick: function () {
            this.state.start("StartScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        }
    };

}());