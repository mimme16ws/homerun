var HomeRun = HomeRun || {};

/* eslint-env browser */

/* Zu diesem Screen gelangt man, indem man im Startmenü auf den Button Einstellungen klickt, hier hat man die Auswahl zwischen 3 verschiedenen Charakteren, über die man hovern kann (kleiner schwarzer Rang beabsichtigt, um Auswahlmöglichkeit darzustellen) und die nach Auswählen dann auch im Spiel spielbar sind. Desweiteren gibt es die Möglichkeit, die Hintergrundmusik sowie Töne im Spiel stumm zu stellen. */

(function () {
    "use strict";

    HomeRun.SettingsScreen = function () {};

    HomeRun.SettingsScreen.prototype = {
        init: function (chosenCharacter, musikActivated, soundActivated) {
            this.chosenCharacter = chosenCharacter;
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
        },
        
        preload: function () {},

        create: function () {
            this.startScreenBackground = this.add.sprite(400, 300, "startScreen");
            this.startScreenBackground.anchor.setTo(0.5, 0.5);
            
            this.clickHinweise = this.add.sprite(320, 553, "clickHinweise");
            this.clickHinweise.scale.setTo(1, 1);
            
            this.zurueckButton = this.game.add.button(220, 100, "zurueck", this.zurueckButtonOnClick, this);
            this.zurueckButton.anchor.setTo(1, 1);
            
            this.charakterauswahl = this.add.sprite(220, 250, "charakterauswahl");
            this.charakterauswahl.scale.setTo(0.4, 0.4);
            
            this.chooseHomer = this.game.add.button(300, 390, "chooseHomerHover", this.homerOnClick, this, 0, 1);
            this.chooseHomer.anchor.setTo(0.7, 0.5);
            this.chooseHomer.scale.setTo(0.8, 0.8);
            
            this.chooseMarge = this.game.add.button(400, 390, "chooseMargeHover", this.margeOnClick, this, 0, 1);
            this.chooseMarge.anchor.setTo(0.7, 0.5);
            this.chooseMarge.scale.setTo(0.8, 0.8);
            
            this.chooseBart = this.game.add.button(500, 390, "chooseBartHover", this.bartOnClick, this, 0, 1);
            this.chooseBart.anchor.setTo(0.7, 0.5);
            this.chooseBart.scale.setTo(0.8, 0.8);
            
            if (this.musikActivated === undefined || this.musikActivated === true) {
                this.musikActivated = true;
                this.musik = this.game.add.button(210, 553, "musikButton", this.musikButtonClicked, this);
                this.musik.scale.setTo(0.24, 0.24);
            } else {
                this.musik = this.game.add.button(210, 553, "musikButtonDeaktiviert", this.musikButtonClicked, this);
                this.musik.scale.setTo(0.24, 0.24);
            }
            
            if (this.soundActivated === undefined || this.soundActivated === true) {
                this.soundActivated = true;
                this.sound = this.game.add.button(485, 553, "soundButton", this.soundButtonClicked, this);
                this.sound.scale.setTo(0.24, 0.24);
            } else {
                this.sound = this.game.add.button(485, 553, "soundButtonDeaktiviert", this.soundButtonClicked, this);
                this.sound.scale.setTo(0.24, 0.24);
            }
        },

        zurueckButtonOnClick: function () {
            this.state.start("StartScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },
        
        musikButtonClicked: function () {
            if (this.musikActivated === true) {
                this.musik.loadTexture("musikButtonDeaktiviert");
                this.musikActivated = false;
            } else {
                this.musik.loadTexture("musikButton");
                this.musikActivated = true;
            }
        },

        soundButtonClicked: function () {
            if (this.soundActivated === true) {
                this.sound.loadTexture("soundButtonDeaktiviert");
                this.soundActivated = false;
            } else {
                this.sound.loadTexture("soundButton");
                this.soundActivated = true;
            }
        },

        homerOnClick: function () {
            this.state.start("StartScreen", true, false, "homer", this.musikActivated, this.soundActivated);
        },

        margeOnClick: function () {
            this.state.start("StartScreen", true, false, "marge", this.musikActivated, this.soundActivated);
        },

        bartOnClick: function () {
            this.state.start("StartScreen", true, false, "bart", this.musikActivated, this.soundActivated);
        }
    };
    
}());