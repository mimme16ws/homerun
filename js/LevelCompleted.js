var HomeRun = HomeRun || {};

/* eslint-env browser */

/* In dieser Methode wird der LevelCompleted-Screen aufgerufen, sprich der Screen in welchem nach Beendigung eines Levels eine Meldung erscheint und man die Möglichkeit hat, das Spiel neuzustarten sowie ins Hauptmenü zurückzukehren */

(function () {
    "use strict";

    HomeRun.LevelCompleted = function () {};

    HomeRun.LevelCompleted.prototype = {
        init: function (chosenCharacter, level, musikActivated, soundActivated) {
            this.chosenCharacter = chosenCharacter;
            this.level = level;
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
        },
        
        preload: function () {},

        create: function () {
            this.levelCompletedText = this.add.sprite(400, 250, "levelCompleted");
            this.levelCompletedText.anchor.setTo(0.5, 0.5);

            this.neustartButton = this.game.add.button(170, 400, "neustart", this.neustartButtonOnClick, this);
            this.neustartButton.scale.setTo(0.5, 0.5);

            this.hauptmenueButton = this.game.add.button(430, 400, "hauptmenue", this.hauptmenueButtonOnClick, this);
            this.hauptmenueButton.scale.setTo(0.5, 0.5);
        },

        neustartButtonOnClick: function () {
            this.state.start("Game", true, false, this.level, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        hauptmenueButtonOnClick: function () {
            this.state.start("StartScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        }
    };
    
}());