var HomeRun = HomeRun || {};
var result;
var Phaser;

/* eslint-env browser */

/* In diesem State wird die Spiellogik implementiert, alle Objekte erzeugt und die weitergehenden States aufgerufen */

(function () {
    "use strict";

    HomeRun.Game = function () {};

    HomeRun.Game.prototype = {
        
        /* Initialisiert die übergebenen Parameter */
        init: function (level, chosenCharacter, musikActivated, soundActivated) {
            this.level = level;
            this.chosenCharacter = chosenCharacter;
            if (musikActivated === undefined) {
                this.musikActivated = true;
            } else {
                this.musikActivated = musikActivated;
            }
            if (soundActivated === undefined) {
                this.soundActivated = true;
            } else {
                this.soundActivated = soundActivated;
            }
        },

        create: function () {
            /* Ausgewählte Map wird geladen */
            this.selectMap();

            /* Tiledlayers werden erzeugt und angezeigt */
            this.backgroundLayer = this.map.createLayer("Background");
            this.backgroundObjectsLayer = this.map.createLayer("BackgroundObjects");
            this.bodenLayer = this.map.createLayer("Boden");
            this.platformLayer = this.map.createLayer("Platformen");
            this.kraterLayer = this.map.createLayer("Krater");
            this.enemyBoundsLayer = this.map.createLayer("enemyBounds");
            this.enemyBoundsLayer.alpha = 0;
            
            /* Powerupstate initialisieren */
            this.gotPowerup = false;
            
            /* Deathstate initialisieren */
            this.playerDeath = false;

            /* Collision mit Boden */
            this.map.setCollisionBetween(1, 10000, true, "Boden");

            /* Collision mit Platformen */
            this.map.setCollisionBetween(1, 10000, true, "Platformen");

            /* Collision mit EnemyStoppEbene */
            this.map.setCollisionBetween(1, 10000, true, "enemyBounds");

            /* Welt wird auf den Bildschirm resized */
            this.backgroundLayer.resizeWorld();

            /* Falls Musik aktiviert, wird Musik erstellt */
            if (this.musikActivated === true) {
                this.createAudio();
            }

            /* Donuts erstellen */
            this.createDonuts();
            
            /* Powerups erstellen */
            this.createPowerups();

            /* goldener Donut als Levelende wird erstellt */
            this.createBigDonut();

            /* Fallen erstellt */
            this.createTraps();

            /* Gegner erstellen */
            this.createEnemy();

            /* Scoretext erstellen */
            this.score = 0;
            this.scoreText = this.game.add.text(16, 16, "Score: 0", { fontSize: "32px", fill: "#000" });
            this.scoreText.fixedToCamera = true;

            /* Spieler erstellen */
            this.createPlayer();

            /* Kamera auf Spieler zentrieren */
            this.game.camera.follow(this.player);

            /* Keyboardinput initialisieren */
            this.cursors = this.game.input.keyboard.createCursorKeys();
        },

        /* Methode zur Levelselektierung */
        selectMap: function () {
            switch (this.level) {
            case 1:
                this.map = this.game.add.tilemap("firstLevel");
                this.map.addTilesetImage("gameTiles", "gameTiles");
                break;

            case 2:
                this.map = this.game.add.tilemap("secondLevel");
                this.map.addTilesetImage("gameTiles2", "gameTiles2");
                break;

            case 3:
                this.map = this.game.add.tilemap("thirdLevel");
                this.map.addTilesetImage("gameTiles3", "gameTiles3");
                break;
                    
            case 4:
                this.map = this.game.add.tilemap("tutorialLevel");
                this.map.addTilesetImage("TutorialTilemap", "TutorialTilemap");
                this.map.addTilesetImage("spielAnweisungen", "spielAnweisungen");
                break;
                    
            default:
                break;
            }
        },

        /* Audio wird erstellt */
        createAudio: function () {
            this.music = this.game.add.audio("simpsonsTheme", true);
            this.music.play();
        },
        
        /* Player wird erstellt, Phaser Physik aktiviert, Kollsion mit Weltrand gesetzt und Animationen erstellt */
        createPlayer: function () {
            result = this.findObjectsByType("playerStart", this.map, "Player");
            this.selectChosenCharacter();
            this.game.physics.arcade.enable(this.player);
            this.player.body.gravity.y = 500;
            this.player.body.collideWorldBounds = true;
            this.player.animations.add("left", [0, 1, 2, 3, 4, 5, 6, 7], 10, true);
            this.player.animations.add("right", [16, 15, 14, 13, 12, 11, 10, 9], 10, true);
        },
        
        /* Der im Settingsstate gewählte Charakter wird als Sprite geladen */
        selectChosenCharacter: function () {
            switch (this.chosenCharacter) {
            case "homer":
                this.player = this.game.add.sprite(result[0].x, result[0].y - 50, "homer");
                this.player.scale.setTo(0.75, 0.75);
                break;
                    
            case "marge":
                this.player = this.game.add.sprite(result[0].x, result[0].y - 50, "marge");
                this.player.scale.setTo(0.65, 0.65);
                break;
                    
            case "bart":
                this.player = this.game.add.sprite(result[0].x, result[0].y - 50, "bart");
                this.player.scale.setTo(0.75, 0.75);
                break;
            
            default:
                break;
            }
        },

        /* Donuts werden erstellt */
        createDonuts: function () {
            this.donuts = this.game.add.group();
            this.donuts.enableBody = true;
            result = this.findObjectsByType("donut", this.map, "Donuts");
            result.forEach(function (e) {
                this.createSpritesForObject(e, this.donuts);
            }, this);
        },
        
        /* Powerups werden erstellt */
        createPowerups: function () {
            this.powerup = this.game.add.group();
            this.powerup.enableBody = true;
            result = this.findObjectsByType("powerup", this.map, "PowerUps");
            result.forEach(function (e) {
                this.createSpritesForObject(e, this.powerup);
            }, this);
        },

        /* großer, goldener Donut wird am Levelende erstellt */
        createBigDonut: function () {
            result = this.findObjectsByType("bigDonut", this.map, "BigDonut");
            this.bigDonut = this.game.add.sprite(result[0].x, result[0].y, "bigDonut");
            this.bigDonut.scale.setTo(2, 2);
            this.game.physics.arcade.enable(this.bigDonut);
        },

        /* Fallen in Form von Atomfässern werden erstellt */
        createTraps: function () {
            this.traps = this.game.add.group();
            this.traps.enableBody = true;
            result = this.findObjectsByType("falle", this.map, "Fallen");
            result.forEach(function (e) {
                this.createSpritesForObject(e, this.traps);
            }, this);
            this.traps.setAll("body.gravity.y", 300);
            this.traps.setAll("body.immovable", true);
        },

        /* Gegner werden erstellt, für jedes Element der group enemies wird eine animation erstellt und bestimmte Werte zur Physik gesetzt */
        createEnemy: function () {
            this.enemies = this.game.add.group();
            this.enemies.enableBody = true;
            result = this.findObjectsByType("enemy", this.map, "Enemy");
            result.forEach(function (e) {
                this.createSpritesForObject(e, this.enemies);
            }, this);
            this.enemies.setAll("scale.y", 1.5);
            this.enemies.setAll("scale.x", 1.5);
            this.enemies.setAll("body.gravity.y", 300);
            this.enemies.setAll("body.immovable", true);
            this.enemies.setAll("body.velocity.x", -100);
            this.enemies.callAll("animations.add", "animations", "left", [3, 2, 1, 0], 10, true);
            this.enemies.callAll("animations.add", "animations", "right", [8, 7, 6, 5], 10, true);
            this.enemies.callAll("play", null, "left");
        },
        
        /* Es werden die Sprites für die in Tiles erstellten Objekte geladen (nur für Objekte die nicht spezifisch im Code ein Sprite bekommen) */
        createSpritesForObject: function (element, group) {
            /* Erstellt ein Sprite für ein Tiledelement */
            var sprite = group.create(element.x, element.y, element.properties.sprite);

            /* Properties werden auf entsprechendes Sprite gesetzt */
            Object.keys(element.properties).forEach(function (key) {
                sprite[key] = element.properties[key];
            });
        },

        /* Aus der Dokumentation übernommen Funktion um die Mapdateien nach Objekten bestimmten Typs zu filtern und diese zurückzugeben */
        findObjectsByType: function (type, map, layer) {
            var result = [];
            map.objects[layer].forEach(function (element) {
                if (element.properties.type === type) {
                    element.y -= map.tileHeight;
                    result.push(element);
                }
            });
            return result;
        },

        /* Lässt den Spieler springen */
        jumpPlayer: function () {
            if (this.player.body.onFloor()) {
                this.player.body.velocity.y -= 500;
            }
        },

        /* Leitet den Tod des Spielers in die Wege, speichert den Highscore und nach zwei Sekunden wir dann GameOver aufgerufen */
        playerDied: function () {
            this.textureString = this.chosenCharacter + "Death";
            this.player.loadTexture(this.textureString);
            if (this.soundActivated) {
                this.homerDeathSound = this.game.add.audio("homerDeathSound");
                this.homerDeathSound.play();
            }
            this.setHighscore();
            this.player.body.collideWorldBounds = false;
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.player.body.velocity.y = -150;
            this.game.time.events.add(Phaser.Timer.SECOND * 2, this.startGameOver, this);
            if (this.musikActivated) {
                this.music.stop();
            }
        },
        
        /* GameOverState starten */
        startGameOver: function () {
            this.state.start("GameOver", true, false, this.chosenCharacter, this.level, this.musikActivated, this.soundActivated);
        },
        
        /* aufgerufen bei Collision mit einer Falle, Player stirbt falls er kein Powerup hat */
        hitTrap: function () {
            if (this.gotPowerup === false && this.playerDeath === false) {
                this.playerDeath = true;
                this.playerDied();
            } else {
                this.game.time.events.add(Phaser.Timer.SECOND, this.setPowerupFalse, this);
            }
        },

        /* aufgerufen bei Collision mit einem Gegner, Player stirbt falls er kein Powerup hat */
        hitEnemy: function (player, enemy) {
            if (player.body.touching.down) {
                enemy.kill();
                enemy.animations.stop();
                enemy.frame = 4;
            } else {
                if (this.gotPowerup === false && this.playerDeath === false) {
                    this.playerDeath = true;
                    this.playerDied();
                } else {
                    this.game.time.events.add(Phaser.Timer.SECOND, this.setPowerupFalse, this);
                }
            }
        },
        
        /* Falls Player einen Gegner oder eine Falle berührt wird, gefolgt von einer einsekündigen Unverwundbarkeit, sein Powerup entfernt  */
        setPowerupFalse: function () {
            this.player.loadTexture(this.chosenCharacter);
            this.gotPowerup = false;
        },

        /* Falls der Gegner mit der EnemyBoundsLayer kollidiert, ändert er seine Richtung */
        enemyChangesDirection: function (enemy) {
            if (enemy.body.blocked.right) {
                enemy.animations.play("left");
                enemy.body.velocity.x = -100;
            }
            if (enemy.body.blocked.left) {
                enemy.animations.play("right");
                enemy.body.velocity.x = +100;
            }
        },

        /* Aufruf bei Bestehen des Levels, highscore wird gesetzt und LevelCompleteState gestartet */
        endLevel: function () {
            this.setHighscore();
            this.state.start("LevelCompleted", true, false, this.chosenCharacter, this.level, this.musikActivated, this.soundActivated);
            if (this.musikActivated) {
                this.music.stop();
            }
        },
        
        /* Falls der aktuelle Score höher als der zZ vorhandene Score im LocalStorage ist, wird der aktuelle Score in den LocalStorage gepushed */
        setHighscore: function () {
            this.levelScore = "highScoreLevel" + this.level;
            if (localStorage.getItem(this.levelScore) < this.score) {
                localStorage.setItem(this.levelScore, this.score);
            }
        },

        /* Updatefunktion von Phaser, die dauerhaft aufgerufen wird */
        update: function () {
            /* Todesabfrage falls der Spieler in einen Krater fällt */
            if (this.player.bottom === this.game.world.bounds.bottom) {
                this.playerDied();
            }

            /* Atomfässer Kollision mit Welt */
            this.game.physics.arcade.collide(this.traps, this.platformLayer);
            this.game.physics.arcade.collide(this.traps, this.bodenLayer);

            /* Gegner Kollision mit Welt */
            this.game.physics.arcade.collide(this.enemies, this.platformLayer);
            this.game.physics.arcade.collide(this.enemies, this.bodenLayer);

            /* Gegner Kollision mit EnemyBoundsLayer */
            this.game.physics.arcade.collide(this.enemies, this.enemyBoundsLayer, this.enemyChangesDirection, null, this);

            /* Player Kollision mit Welt, falls er am Leben ist */
            if (this.playerDeath === false) {
                this.game.physics.arcade.collide(this.player, this.bodenLayer);
                this.game.physics.arcade.collide(this.player, this.platformLayer);
            }

            /* Player Kollision mit Fallen */
            this.game.physics.arcade.overlap(this.player, this.traps, this.hitTrap, null, this);

            /* Player Kollision mit Gegner */
            this.game.physics.arcade.overlap(this.player, this.enemies, this.hitEnemy, null, this);

           /* Player Kollision mit Donut */
            this.game.physics.arcade.overlap(this.player, this.donuts, this.collectDonuts, null, this);
            
            /* Player Kollision mit Powerup */
            this.game.physics.arcade.overlap(this.player, this.powerup, this.collectPowerup, null, this);

            /* Player Kollision mit goldenem Donut */
            this.game.physics.arcade.overlap(this.player, this.bigDonut, this.endLevel, null, this);

            /* PlayerMovement mit Tastatureingabe */
            this.player.body.velocity.x = 0;

            if (this.cursors.up.isDown) {
                this.jumpPlayer();
            }
            if (this.cursors.left.isDown) {
                this.player.body.velocity.x -= 300;
                this.player.animations.play("left");
            } else if (this.cursors.right.isDown) {
                this.player.body.velocity.x += 300;
                this.player.animations.play("right");
            } else {
                this.player.animations.stop();
                this.player.frame = 8;
            }
        },
        
        /* Player kollidiert mit Powerup und erhält Unverwundbarkeit, Powerup zerstört */
        collectPowerup: function (player, powerupCollected) {
            powerupCollected.destroy();
            this.powerupCharacter = this.chosenCharacter + "Powerup";
            this.player.loadTexture(this.powerupCharacter);
            this.gotPowerup = true;
        },

        /* Player kollidiert mit Donut, erhält 10 Punkte und Donut wird zerstört */
        collectDonuts: function (player, donutCollected) {
            donutCollected.destroy();
            this.score += 10;
            this.scoreText.text = "Score: " + this.score;
        }
    };

}());
