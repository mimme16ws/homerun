var HomeRun = HomeRun || {};

/* eslint-env browser */

/* Dieser Screen wird nach Abspielen eines kurzen Ladebildschirms aufgerufen und stellt das Hauptmenü dar, durch das man in alle unteren Bildschirme navigieren kann mithilfe von Buttons. */

(function () {
    "use strict";

    HomeRun.StartScreen = function () {};
    
    /* ES-Lint Fehler: !== erwartet, bei Behebung funktioniert aber Code nicht mehr, Charakter nicht mehr wählbar */
    
    HomeRun.StartScreen.prototype = {
        init: function (chosenCharacter, musikActivated, soundActivated) {
            this.musikActivated = musikActivated;
            this.soundActivated = soundActivated;
            if (chosenCharacter != null) {
                this.chosenCharacter = chosenCharacter;
            } else {
                this.chosenCharacter = "homer";
            }
        },
                
        preload: function () {},
        
        create: function () {
            this.startScreenBackground = this.add.sprite(400, 300, "startScreen");
            this.startScreenBackground.anchor.setTo(0.5, 0.5);

            this.spielenButton = this.game.add.button(400 - 10, 300 - 20, "spielen", this.spielenButtonOnClick, this);
            this.spielenButton.scale.setTo(0.2, 0.2);
            this.spielenButton.anchor.setTo(0.5, 0.5);

            this.highscoreButton = this.game.add.button(400 - 10, 300 + 40, "highscore", this.highscoreButtonOnClick, this);
            this.highscoreButton.scale.setTo(0.2, 0.2);
            this.highscoreButton.anchor.setTo(0.5, 0.5);

            this.settingsButton = this.game.add.button(400 - 10, 300 + 100, "settings", this.settingsButtonOnClick, this);
            this.settingsButton.scale.setTo(0.2, 0.2);
            this.settingsButton.anchor.setTo(0.5, 0.5);

            this.creditsButton = this.game.add.button(400 - 10, 300 + 160, "credits", this.creditsButtonOnClick, this);
            this.creditsButton.anchor.setTo(0.5, 0.5);
            this.creditsButton.scale.setTo(0.2, 0.2);
        },

        spielenButtonOnClick: function () {
            this.state.start("LevelScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        highscoreButtonOnClick: function () {
            this.state.start("HighscoreScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        settingsButtonOnClick: function () {
            this.state.start("SettingsScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        },

        creditsButtonOnClick: function () {
            this.state.start("CreditsScreen", true, false, this.chosenCharacter, this.musikActivated, this.soundActivated);
        }
    };
    
}());